var xmlhttp = new XMLHttpRequest();
var url = "art.json";
//logic to access the data.json
xmlhttp.onreadystatechange = function () {
	if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
		var myArr = JSON.parse(xmlhttp.responseText);
		list(myArr);
		console.log(myArr)
	}
};
xmlhttp.open("GET", url, true);
xmlhttp.send();
//function to list the content in cards
function list(arr) {
	var out = "";
	var i;
	for (i = 0; i < arr.length; i++) {
		out += "<div id=" + arr[i].ObjectID + " class='card' onclick=saveSelection(" + arr[i].ObjectID + ");>" +
			"<h3>" + arr[i].Title + "</h3>" + "<p>" + arr[i].Artist + "</p>"+"</div>"
	}
	document.getElementById("id01").innerHTML = out;
	
	for (i = 0; i < arr.length; i++) {
		console.log(arr[i].ThumbnailURL);
		var img = document.getElementById(arr[i].ObjectID);
		console.log(img);
		img.style.backgroundImage="url('"+arr[i].ThumbnailURL+"')"
	}
}
//logic to save the selected value
function saveSelection(sel) {
	/* this function is called when the user clicks a list item */
	/* the ID of the list item clicked will be saved in Local Storage */
	// Check browser support of Local Storage
	if (typeof (Storage) !== "undefined") {
		//use localStorage if available
		localStorage.setItem("selected", sel);
	}
	else { 
		// use cookies
		SetCookie("selected",sel);
	}
	window.open('page2Template.html', "_self");
}