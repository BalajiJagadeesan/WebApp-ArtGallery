var xmlhttp = new XMLHttpRequest();
var url = "art.json";
xmlhttp.onreadystatechange = function () {
	if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
		var myArr = JSON.parse(xmlhttp.responseText);
		list(myArr);
	}
};
xmlhttp.open("GET", url, true);
xmlhttp.send();
//logic to get more details about the selected card
function list(arr) {
	var out = "";
	var temp = "";
	var i, j;
	if (!localStorage.getItem("selected")) var sel = GetCookie("selected");
	else var sel = localStorage.getItem("selected");
	for (i = 0; i < arr.length; i++) {
		// look for the item the user selected on page 1 
		if (sel == arr[i].ObjectID) {
			out = "<div class='cardlist'>" + 
				"<img class='card'  src='"+arr[i].ThumbnailURL+"'/>"+
				"<h3> Title : " + arr[i].Title + "</h3>" +
				"<p> Dimensions : " + arr[i].Dimensions + "</p>" +
				"<p> Year of Creation : " + arr[i].Date + "</p>" +
				"<p> Date acquired by MOMA : " + arr[i].DateAcquired + "</p>" +
				"<p> Donated By : " + arr[i].CreditLine + "</p>"+
				"<h3>Artist List:</h3>"
			for (j = 0; j < arr[i]["Artist"].length; j++) {
				temp +="<p> Artist : " + arr[i].Artist[j] + "</p>" + "<p> Artist Biography : " + arr[i].ArtistBio[j] + "</p>" + "<p> Nationality : " + arr[i].Nationality[j] + "</p>" + "<p> Gender : " + arr[i].Gender[j] + "</p>"+"<hr>";
			}
			temp=temp+"<a href='"+arr[i].URL+"'>Main MOMA link</a>"+"</div>";
		}
	}
	// show the user the output by filling the div with ID='id01'
	document.getElementById("id01").innerHTML = out+temp;
}